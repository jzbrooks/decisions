package com.jzbrooks.resolve;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

public class AboutActivity extends AppCompatActivity {

    @SuppressWarnings({"NullableProblems"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        Toolbar toolbar = (Toolbar) findViewById(R.id.mainToolbar);
        setSupportActionBar(toolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        TextView buildTextView = (TextView) findViewById(R.id.build_no_text_view);
        buildTextView.setText("Build Number v" + BuildConfig.VERSION_NAME);

        TextView buildTypeTextView = (TextView) findViewById(R.id.build_type_text_view);
        buildTypeTextView.setText("Build Type: {" + BuildConfig.BUILD_TYPE + "}");
    }

}
