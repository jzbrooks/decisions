package com.jzbrooks.resolve;

import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.TextView;

import com.jzbrooks.resolve.data.DataModel;
import com.jzbrooks.resolve.services.Frequency;

import org.adw.library.widgets.discreteseekbar.DiscreteSeekBar;

/**
 * Event Option Dialog Fragment for adding / modifying event options
 */
public class ActivityOptionDialogFragment extends DialogFragment implements DiscreteSeekBar.OnProgressChangeListener {

    private Activity hostActivity;
    private View rootView;
    private EditText optionNameView;
    private DiscreteSeekBar optionFreqView;
    private String activityGroup;
    private String activityOption;
    private String activityWeight;
    private int activityOptionPosition;
    private boolean isBeingModified;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.event_option_dialog_layout, container, false);

        rootView.setFocusableInTouchMode(true);
        rootView.requestFocus();
        rootView.setOnKeyListener(new View.OnKeyListener() {

            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                if (i == KeyEvent.KEYCODE_BACK) {
                    ((ActivityOptionActivity)getActivity()).setupActionBar();
                    dismiss();
                    return true;
                }

                return false;
            }
        });

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();

        // read inputs
        hostActivity = getActivity();

        activityGroup = getArguments().getString(ActivityOptionActivity.activityGroupArgKey);
        activityOption = getArguments().getString(ActivityOptionActivity.activityOptionArgKey);
        activityWeight = getArguments().getString(ActivityOptionActivity.activityWeightArgKey);
        activityOptionPosition = getArguments().getInt(ActivityOptionActivity.activityAdapterPosition);

        if (activityOption != null) {
            isBeingModified = true;
        }

        // setup ui
        ActionBar actionBar = ((AppCompatActivity) hostActivity).getSupportActionBar();
        if (actionBar != null) {
            if (isBeingModified) {
                actionBar.setTitle(String.format(getResources().getString(R.string.modify_activity_option_dialog_title), activityGroup));
            }
            else {
                actionBar.setTitle(String.format(getResources().getString(R.string.new_activity_option_dialog_title), activityGroup));
            }
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
            Drawable cancelIcon = getResources().getDrawable(R.drawable.ic_close_material);
            DrawableCompat.setTint(cancelIcon, getResources().getColor(R.color.colorAccentWhite));
            actionBar.setHomeAsUpIndicator(cancelIcon);
        }

        this.setHasOptionsMenu(true);

        // prep input fields
        optionNameView = (EditText) rootView.findViewById(R.id.optionName);
        if (isBeingModified) {
            optionNameView.setText(activityOption);
        }

        optionFreqView = (DiscreteSeekBar) rootView.findViewById(R.id.optionFreq);
        optionFreqView.setOnProgressChangeListener(this);
        if (isBeingModified) {
            int progressValue = Frequency.convertDbValueToProgress(Integer.parseInt(activityWeight));
            optionFreqView.setProgress(progressValue);
            if (progressValue == 1) {
                TextView textView = (TextView) rootView.findViewById(R.id.eventFreqDescription);
                textView.setText(R.string.less_frequent);
            }
        }
        else {
            optionFreqView.setProgress(Frequency.Frequent);
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        getActivity().getMenuInflater().inflate(R.menu.new_event_option_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_save) {
            Save();
            dismiss();
            ((ActivityOptionActivity) hostActivity).setupActionBar();
            return true;
        } else if (id == android.R.id.home) {
            dismiss();
            ((ActivityOptionActivity) hostActivity).setupActionBar();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void Save() {
        final String optionName = optionNameView.getText().toString();
        int optionProgressValue = optionFreqView.getProgress();

        final String prevEventOption = activityOption;
        final String prevEventWeight = activityWeight;

        String message;

        View parentView = getActivity().findViewById(R.id.eventOptionCoordinator);

        if (optionName.isEmpty()) {
            Snackbar.make(parentView,"Option Name Can't Be Empty", Snackbar.LENGTH_LONG)
                    .show();

            return;
        }

        final int optionWeight = Frequency.convertProgressToDbValue(optionProgressValue);

        if (isBeingModified) {
            message = String.format(getString(R.string.modified_item), prevEventOption);
            DataModel.ModifyActivityOption(activityGroup, activityOption, activityWeight, optionName, Integer.toString(optionWeight));
            ((ActivityOptionActivity) hostActivity).recyclerViewAdapter.notifyItemChanged(activityOptionPosition);
        }
        else {
            message = String.format(getString(R.string.added_item), optionName);
            DataModel.AddActivityOption(activityGroup, optionName, Integer.toString(optionWeight));
        }

        InputMethodManager imm = (InputMethodManager)hostActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(rootView.getWindowToken(), 0);

        Snackbar.make(parentView, message, Snackbar.LENGTH_LONG)
                .setAction(R.string.undo, new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (isBeingModified) {
                            DataModel.ModifyActivityOption(activityGroup, optionName, Integer.toString(optionWeight), prevEventOption, prevEventWeight);
                            ((ActivityOptionActivity) hostActivity).recyclerViewAdapter.notifyItemChanged(activityOptionPosition);
                        }
                        else {
                            DataModel.DeleteActivityOption(activityGroup, optionName);
                            ((ActivityOptionActivity) hostActivity).recyclerViewAdapter.notifyItemRemoved(activityOptionPosition);
                            ((ActivityOptionActivity) hostActivity).recyclerViewAdapter.notifyItemRangeChanged(activityOptionPosition,
                                    DataModel.eventOptionList.get(activityGroup).size());
                        }
                    }
                }).show();
    }

    @Override
    public void onProgressChanged(DiscreteSeekBar seekBar, int value, boolean fromUser) {
        if (seekBar == optionFreqView) {
            TextView textView = (TextView) rootView.findViewById(R.id.eventFreqDescription);
            if (value == Frequency.LessFrequent) {
                textView.setText(R.string.less_frequent);
            }
            if (value == Frequency.Frequent) {
                textView.setText(R.string.frequent);
            }
            if (value == Frequency.MoreFrequent) {
                textView.setText(R.string.more_frequent);
            }
        }
    }

    @Override
    public void onStartTrackingTouch(DiscreteSeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(DiscreteSeekBar seekBar) {

    }
}
