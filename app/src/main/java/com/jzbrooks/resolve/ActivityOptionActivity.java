package com.jzbrooks.resolve;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.jzbrooks.resolve.data.DataModel;
import com.jzbrooks.resolve.services.DeletableListAdapter;

public class ActivityOptionActivity extends AppCompatActivity {
    private String activityGroup = null;

    public DeletableListAdapter recyclerViewAdapter;

    public static String activityGroupArgKey = "eventGroup";
    public static String activityOptionArgKey = "eventOption";
    public static String activityWeightArgKey = "eventWeight";
    public static String activityAdapterPosition = "activityAdapterPosition";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_option);

        SharedPreferences sharedPreferences = getPreferences(Context.MODE_PRIVATE);
        boolean tutorialDismissedForever = sharedPreferences.getBoolean(getString(R.string.saved_activity_options_tutorial_state), false);
        if (!tutorialDismissedForever) {
            final Dialog dialog = new Dialog(this);
            dialog.setTitle(R.string.what_now);
            dialog.setContentView(R.layout.tutorial_dialog_layout);

            TextView textView = (TextView) dialog.findViewById(R.id.tutorial_text);
            Button dismissBtn = (Button) dialog.findViewById(R.id.dismiss_btn);
            Button dismissForeverBtn = (Button) dialog.findViewById(R.id.dismiss_forever_btn);

            textView.setText(String.format(getString(R.string.activity_option_tutorial), activityGroup));

            dismissBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            dismissForeverBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SharedPreferences sharedPref = getPreferences(Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPref.edit();
                    editor.putBoolean(getString(R.string.saved_activity_options_tutorial_state), true);
                    editor.apply();

                    dialog.dismiss();
                }
            });

            dialog.show();
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if (DataModel.eventList == null) {
            DataModel.initialize(this);
        }

        Intent intent = getIntent();
        activityGroup = intent.getStringExtra("EventGroup");

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        if (fab != null) {
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    DialogFragment eventOptionDialog = new ActivityOptionDialogFragment();
                    Bundle dialogArgs = new Bundle();
                    dialogArgs.putString(activityGroupArgKey, activityGroup);
                    dialogArgs.putInt(activityAdapterPosition, DataModel.DbGetActivityOptions(activityGroup).size());
                    eventOptionDialog.setArguments(dialogArgs);

                    FragmentManager fragmentManager = getFragmentManager();
                    FragmentTransaction transaction = fragmentManager.beginTransaction();
                    transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                    transaction.add(android.R.id.content, eventOptionDialog).addToBackStack(null).commit();
                }
            });
        }

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle(String.format(getResources().getString(R.string.actionbar_title_activity_option), activityGroup));
        }

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.list_view);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(llm);

        ItemTouchHelper touchHelper = new ItemTouchHelper(itemTouchCallback);
        touchHelper.attachToRecyclerView(recyclerView);

        recyclerViewAdapter = new DeletableListAdapter(DataModel.eventOptionList.get(activityGroup));
        recyclerViewAdapter.setLongClickListener(itemLongClickListener);

        recyclerView.setAdapter(recyclerViewAdapter);
        recyclerView.setLongClickable(true);
    }

    public void setupActionBar() {
        ActionBar bar = getSupportActionBar();
        if (bar != null) {
            bar.setHomeAsUpIndicator(null);
            bar.setTitle(String.format(getResources().getString(R.string.actionbar_title_activity_option), activityGroup));
        }
    }

    public void invalidateList() {
        recyclerViewAdapter.notifyDataSetChanged();
    }

    private View.OnLongClickListener itemLongClickListener = new View.OnLongClickListener() {
        @Override
        public boolean onLongClick(View view) {
            String option;
            String optionWeight;
            DeletableListAdapter.DeletableViewHolder viewHolder;
            DialogFragment eventOptionDialog = new ActivityOptionDialogFragment();
            Bundle dialogArgs = new Bundle();

            viewHolder = (DeletableListAdapter.DeletableViewHolder)view.getTag();
            option = ((TextView)view.findViewById(R.id.list_item_text)).getText().toString();
            optionWeight = DataModel.DbGetActivityOptionWeight(activityGroup, option);

            dialogArgs.putString(activityGroupArgKey, activityGroup);
            dialogArgs.putString(activityOptionArgKey, option);
            dialogArgs.putString(activityWeightArgKey, optionWeight);
            dialogArgs.putInt(activityAdapterPosition, viewHolder.getAdapterPosition());
            eventOptionDialog.setArguments(dialogArgs);

            FragmentManager fragmentManager = ActivityOptionActivity.this.getFragmentManager();
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
            transaction.add(android.R.id.content, eventOptionDialog).addToBackStack(null).commit();

            return true;
        }

    };

    ItemTouchHelper.SimpleCallback itemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.RIGHT | ItemTouchHelper.LEFT) {

        Drawable background;
        Drawable deleteIcon;
        int deleteIconMargin;
        boolean initialized;

        private void intialize() {
            background = new ColorDrawable(ActivityOptionActivity.this.getResources().
                    getColor(R.color.colorAccent));
            deleteIcon = ContextCompat.getDrawable(ActivityOptionActivity.this, R.drawable.ic_delete_material);
            deleteIcon.setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_ATOP);
            deleteIconMargin = (int) ActivityOptionActivity.this.getResources().getDimension(R.dimen.delete_icon_padding);
            initialized = true;
        }

        @Override
        public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
            return false;
        }

        @Override
        public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {

            TextView textView = ((DeletableListAdapter.DeletableViewHolder)viewHolder).mNameView;
            final String item = textView.getText().toString();
            final String eventOptionWeight = DataModel.DbGetActivityOptionWeight(activityGroup, item);
            final int removedPosition = viewHolder.getAdapterPosition();

            DataModel.DeleteActivityOption(activityGroup, item);

            recyclerViewAdapter.notifyItemRemoved(removedPosition);
            recyclerViewAdapter.notifyItemRangeChanged(removedPosition, DataModel.eventOptionList.get(activityGroup).size());

            Snackbar.make(viewHolder.itemView.getRootView().findViewById(R.id.list_view), String.format(getString(R.string.deleted_item), item), Snackbar.LENGTH_LONG)
                    .setAction(R.string.undo, new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    DataModel.AddActivityOption(activityGroup, item, eventOptionWeight, removedPosition);

                    recyclerViewAdapter.notifyItemInserted(removedPosition);
                    recyclerViewAdapter.notifyItemRangeChanged(removedPosition, DataModel.eventOptionList.get(activityGroup).size());
                }
            }).show();
        }

        @Override
        public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {

            View itemView = viewHolder.itemView;

            if (viewHolder.getAdapterPosition() == -1) {
                return;
            }

            if (!initialized) {
                intialize();
            }

            int itemHeight = itemView.getBottom() - itemView.getTop();
            int intrinsicWidth = deleteIcon.getIntrinsicWidth();
            int intrinsicHeight = deleteIcon.getIntrinsicWidth();

            int deleteIconTop = itemView.getTop() + (itemHeight - intrinsicHeight)/2;
            int deleteIconBottom = deleteIconTop + intrinsicHeight;

            if (itemView.getLeft() + (int) dX < 0) {
                background.setBounds(itemView.getRight() + (int) dX, itemView.getTop(), itemView.getRight(), itemView.getBottom());
                background.draw(c);

                int deleteIconLeft = itemView.getRight() - deleteIconMargin - intrinsicWidth;
                int deleteIconRight = itemView.getRight() - deleteIconMargin;

                Path clipBounds = new Path();
                clipBounds.addRect((float) itemView.getRight() + dX,
                        (float) deleteIconTop,
                        (float) deleteIconRight,
                        (float) deleteIconBottom,
                        Path.Direction.CCW);

                deleteIcon.setBounds(deleteIconLeft, deleteIconTop, deleteIconRight, deleteIconBottom);
                c.clipPath(clipBounds);
                deleteIcon.draw(c);
            }
            else {
                background.setBounds(itemView.getLeft(), itemView.getTop(), itemView.getLeft() + (int) dX, itemView.getBottom());
                background.draw(c);

                int deleteIconLeft = itemView.getLeft() + deleteIconMargin;
                int deleteIconRight = itemView.getLeft() + deleteIconMargin + intrinsicWidth;

                Path clipBounds = new Path();
                clipBounds.addRect((float) deleteIconLeft,
                        (float) deleteIconTop,
                        (float) itemView.getLeft() + dX,
                        (float) deleteIconBottom,
                        Path.Direction.CCW);

                deleteIcon.setBounds(deleteIconLeft, deleteIconTop, deleteIconRight, deleteIconBottom);
                c.clipPath(clipBounds);
                deleteIcon.draw(c);
            }

            super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
        }
    };
}
