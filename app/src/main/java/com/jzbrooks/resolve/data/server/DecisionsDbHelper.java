package com.jzbrooks.resolve.data.server;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * SQLite Schema and Creation
 */
public class DecisionsDbHelper extends SQLiteOpenHelper {
    public static final int DATABASE_VERSION = 5;
    public static final String DATABASE_NAME = "Decisions.db";

    private static final String TEXT_TYPE = " TEXT";
    private static final String COMMA_SEP = ",";
    private static final String SQL_CREATE_EVENTS =
            "CREATE TABLE " + EventEntryContract.EventEntry.TABLE_NAME + " (" +
                    EventEntryContract.EventEntry._ID + " INTEGER PRIMARY KEY," +
                    EventEntryContract.EventEntry.COLUMN_NAME_EVENT + TEXT_TYPE +
                " )";
    private static final String SQL_CREATE_EVENT_OPTIONS =
            "CREATE TABLE " + EventOptionEntryContract.EventOptionEntry.TABLE_NAME + " (" +
                    EventOptionEntryContract.EventOptionEntry._ID + " INTEGER PRIMARY KEY," +
                    EventOptionEntryContract.EventOptionEntry.COLUMN_NAME_EVENT_OPTION + TEXT_TYPE + COMMA_SEP +
                    EventOptionEntryContract.EventOptionEntry.COLUMN_NAME_EVENT_OPTION_WEIGHT + TEXT_TYPE + COMMA_SEP +
                    EventOptionEntryContract.EventOptionEntry.COLUMN_NAME_EVENT + TEXT_TYPE +
                    " )";

    private static final String SQL_DELETE_EVENTS =
            "DROP TABLE IF EXISTS " + EventEntryContract.EventEntry.TABLE_NAME;
    private static final String SQL_DELETE_EVENT_OPTIONS =
            "DROP TABLE IF EXISTS " + EventOptionEntryContract.EventOptionEntry.TABLE_NAME;

    public DecisionsDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        createAll(db);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (newVersion == 5) {
            db.execSQL("ALTER TABLE " + EventOptionEntryContract.EventOptionEntry.TABLE_NAME + " ADD COLUMN " +
                    EventOptionEntryContract.EventOptionEntry.COLUMN_NAME_EVENT_OPTION_WEIGHT + " INTEGER DEFAULT 1");
        }
    }

    public static void dropAll(SQLiteDatabase db) {
        db.execSQL(SQL_DELETE_EVENTS);
        db.execSQL(SQL_DELETE_EVENT_OPTIONS);
    }

    public static void createAll(SQLiteDatabase db) {
        db.execSQL(SQL_CREATE_EVENTS);
        db.execSQL(SQL_CREATE_EVENT_OPTIONS);
    }
}
