package com.jzbrooks.resolve.data.server;

import android.provider.BaseColumns;

/**
 * Created by Justin on 3/13/2016.
 */
public final class EventEntryContract {
    public EventEntryContract() {}

    public static abstract class EventEntry implements BaseColumns {
        public static final String TABLE_NAME = "events";
        public static final String COLUMN_NAME_EVENT_ID = "eventId";
        public static final String COLUMN_NAME_EVENT = "eventName";
    }
}
