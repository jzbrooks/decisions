package com.jzbrooks.resolve.data;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.jzbrooks.resolve.data.server.DecisionsDbHelper;
import com.jzbrooks.resolve.data.server.EventEntryContract;
import com.jzbrooks.resolve.data.server.EventOptionEntryContract;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Class to store data in memory and in SQLite database
 */
public class DataModel {
    public static List<String> eventList;
    public static HashMap<String, List<String>> eventOptionList;

    private static Context mContext;
    private static DecisionsDbHelper mDbHelper;
    private static SQLiteDatabase dbWriteableDatabase;

    public static void initialize(Context context) {
        mContext = context;

        mDbHelper = new DecisionsDbHelper(mContext);
        dbWriteableDatabase = mDbHelper.getWritableDatabase();

        eventList = DbGetActivityGroups();

        if (eventOptionList == null) {
            eventOptionList = new HashMap<>();
        }

        for (String event : eventList) {
            List<String> tempEventOptionList = DbGetActivityOptions(event);
            eventOptionList.put(event, tempEventOptionList);
        }
    }

    public static void ResetDatastores() {
        DecisionsDbHelper.dropAll(dbWriteableDatabase);
        DecisionsDbHelper.createAll(dbWriteableDatabase);
        eventOptionList = null;
        initialize(mContext);
    }

    public static void AddActivityGroup(String activityGroup) {
        eventList.add(activityGroup);
        eventOptionList.put(activityGroup, new ArrayList<String>());
        DbAddActivityGroup(activityGroup);
    }

    public static void AddActivityGroup(String activityGroup, int position) {
        eventList.add(position, activityGroup);
        eventOptionList.put(activityGroup, new ArrayList<String>());
        DbAddActivityGroup(activityGroup);
    }

    public static void DeleteActivityGroup(String activityGroup) {
        eventList.remove(activityGroup);
        eventOptionList.get(activityGroup).clear();
        eventOptionList.remove(activityGroup);
        DbDeleteAllOptionsForActivityGroup(activityGroup);
        DbDeleteActivityGroup(activityGroup);
    }

    public static void DeleteActivityOption(String activityGroup, String activityOption) {
        eventOptionList.get(activityGroup).remove(activityOption);
        DbDeleteActivityOption(activityGroup, activityOption);
    }

    public static void AddActivityOption(String activityGroup, String activityOption, String activityWeight) {
        eventOptionList.get(activityGroup).add(activityOption);
        DbAddActivityOption(activityGroup, activityOption, activityWeight);
    }

    public static void AddActivityOption(String activityGroup, String activityOption, String activityWeight, int position) {
        eventOptionList.get(activityGroup).add(position, activityOption);
        DbAddActivityOption(activityGroup, activityOption, activityWeight);
    }

    public static void ModifyActivityOption(String activityGroup, String activityOption, String activityWeight, String newActivityGroup, String newActivityWeight) {
        int location = eventOptionList.get(activityGroup).indexOf(activityOption);
        eventOptionList.get(activityGroup).remove(activityOption);
        eventOptionList.get(activityGroup).add(location, newActivityGroup);
        DbModifyActivityOption(activityGroup, activityOption, activityWeight, newActivityGroup, newActivityWeight);
    }

    private static long DbAddActivityGroup(String activityGroup) {
        long newRowId;
        ContentValues values = new ContentValues();

        values.put(EventEntryContract.EventEntry.COLUMN_NAME_EVENT, activityGroup);

        newRowId = dbWriteableDatabase.insert(EventEntryContract.EventEntry.TABLE_NAME, null, values);

        return newRowId;
    }

    private static void DbDeleteActivityGroup(String activityGroup) {

        String[] sterilizedInputs = sterilizeQueryParameters(activityGroup);
        activityGroup = sterilizedInputs[0];

        String selection = EventEntryContract.EventEntry.COLUMN_NAME_EVENT + " LIKE ?";

        String[] selectionArgs = { activityGroup };

        dbWriteableDatabase.delete(EventEntryContract.EventEntry.TABLE_NAME, selection, selectionArgs);
    }

    public static long DbAddActivityOption(String activityGroup, String activityOption, String activityWeight) {
        long newRowId;
        ContentValues values = new ContentValues();

        values.put(EventOptionEntryContract.EventOptionEntry.COLUMN_NAME_EVENT, activityGroup);
        values.put(EventOptionEntryContract.EventOptionEntry.COLUMN_NAME_EVENT_OPTION, activityOption);
        values.put(EventOptionEntryContract.EventOptionEntry.COLUMN_NAME_EVENT_OPTION_WEIGHT, activityWeight);

        newRowId = dbWriteableDatabase.insert(EventOptionEntryContract.EventOptionEntry.TABLE_NAME, null, values);

        return newRowId;
    }

    public static long DbModifyActivityOption(String activityGroup, String activityOption, String activityWeight, String newActivityOption, String newActivityWeight) {
        long newRowId;
        ContentValues values = new ContentValues();

        String[] sterilizedInputs = sterilizeQueryParameters(activityGroup, activityOption, activityWeight);
        activityGroup = sterilizedInputs[0];
        activityOption = sterilizedInputs[1];
        activityWeight = sterilizedInputs[2];

        values.put(EventOptionEntryContract.EventOptionEntry.COLUMN_NAME_EVENT, activityGroup);
        values.put(EventOptionEntryContract.EventOptionEntry.COLUMN_NAME_EVENT_OPTION, newActivityOption);
        values.put(EventOptionEntryContract.EventOptionEntry.COLUMN_NAME_EVENT_OPTION_WEIGHT, newActivityWeight);

        String whereClause = EventOptionEntryContract.EventOptionEntry.COLUMN_NAME_EVENT + " = '" + activityGroup + "' AND " +
                EventOptionEntryContract.EventOptionEntry.COLUMN_NAME_EVENT_OPTION + " = '" + activityOption + "'";

        newRowId = dbWriteableDatabase.update(EventOptionEntryContract.EventOptionEntry.TABLE_NAME, values, whereClause, null);

        return newRowId;
    }

    private static List<String> DbGetActivityGroups()
    {
        ArrayList<String> list = new ArrayList<>();

        String[] projection = {
                EventEntryContract.EventEntry.COLUMN_NAME_EVENT
        };

        Cursor cursor = dbWriteableDatabase.query(
                EventEntryContract.EventEntry.TABLE_NAME,
                projection,
                null,
                null,
                null,
                null,
                null
        );

        int count = cursor.getCount();
        for (int i=0; i < count; i++) {

            cursor.move(1);
            int columnIndex = cursor.getColumnIndex(EventEntryContract.EventEntry.COLUMN_NAME_EVENT);
            if (columnIndex != -1) {
                String event = cursor.getString(columnIndex);
                list.add(event);
            }
        }

        cursor.close();

        return list;
    }

    private static void DbDeleteActivityOption(String activityGroup, String activityOption) {

        String[] sterilizedInputs = sterilizeQueryParameters(activityGroup, activityOption);
        activityGroup = sterilizedInputs[0];
        activityOption = sterilizedInputs[1];

        String selection = EventOptionEntryContract.EventOptionEntry.COLUMN_NAME_EVENT_OPTION + " LIKE ? AND " + EventOptionEntryContract.EventOptionEntry.COLUMN_NAME_EVENT + " LIKE ?";

        String[] selectionArgs = { activityOption, activityGroup };

        dbWriteableDatabase.delete(EventOptionEntryContract.EventOptionEntry.TABLE_NAME, selection, selectionArgs);
    }

    private static void DbDeleteAllOptionsForActivityGroup(String acvivityGroup) {

        String[] sterilizedInputs = sterilizeQueryParameters(acvivityGroup);
        acvivityGroup = sterilizedInputs[0];

        String selection = EventOptionEntryContract.EventOptionEntry.COLUMN_NAME_EVENT + " LIKE ?";

        String[] selectionArgs = { acvivityGroup };

        dbWriteableDatabase.delete(EventOptionEntryContract.EventOptionEntry.TABLE_NAME, selection, selectionArgs);
    }

    public static List<String> DbGetActivityOptions(String activityGroup) {
        ArrayList<String> list = new ArrayList<>();

        String[] sterilizedInputs = sterilizeQueryParameters(activityGroup);
        activityGroup = sterilizedInputs[0];

        String[] projection = {
                EventOptionEntryContract.EventOptionEntry.COLUMN_NAME_EVENT_OPTION
        };

        String selection = EventOptionEntryContract.EventOptionEntry.COLUMN_NAME_EVENT + " = ?";
        String[] selectionArgs = new String[]{ activityGroup };

        Cursor cursor = dbWriteableDatabase.query(
                EventOptionEntryContract.EventOptionEntry.TABLE_NAME,
                projection,
                selection,
                selectionArgs,
                null,
                null,
                null
        );

        for (int i=0; i < cursor.getCount(); i++) {
            cursor.move(1);
            int columnIndex = cursor.getColumnIndex(EventOptionEntryContract.EventOptionEntry.COLUMN_NAME_EVENT_OPTION);
            if (columnIndex != -1) {
                String eventOption = cursor.getString(columnIndex);
                list.add(eventOption);
            }
        }

        cursor.close();

        return list;
    }

    public static String DbGetActivityOptionWeight(String activityGroup, String activityOption) {
        String weight;

        String[] sterilizedInputs = sterilizeQueryParameters(activityGroup, activityOption);
        activityGroup = sterilizedInputs[0];
        activityOption = sterilizedInputs[1];

        Cursor cursor = dbWriteableDatabase.rawQuery("SELECT " +
                EventOptionEntryContract.EventOptionEntry.COLUMN_NAME_EVENT_OPTION_WEIGHT + " FROM " + EventOptionEntryContract.EventOptionEntry.TABLE_NAME +
                " WHERE " + EventOptionEntryContract.EventOptionEntry.COLUMN_NAME_EVENT + " = '" + activityGroup + "'" +
                " AND " + EventOptionEntryContract.EventOptionEntry.COLUMN_NAME_EVENT_OPTION + " = '" + activityOption + "'"
                , null);
        cursor.moveToFirst();

        weight = cursor.getString(cursor.getColumnIndex(EventOptionEntryContract.EventOptionEntry.COLUMN_NAME_EVENT_OPTION_WEIGHT));

        cursor.close();

        return weight;
    }

    private static String[] sterilizeQueryParameters(String... params) {
        for (int i=0; i<params.length; ++i) {
            params[i] = params[i].replace("\'", "\'\'");
        }

        return params;
    }
}
