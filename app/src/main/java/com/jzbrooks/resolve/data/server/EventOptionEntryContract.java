package com.jzbrooks.resolve.data.server;

import android.provider.BaseColumns;

/**
 * Created by Justin on 3/13/2016.
 */
public final class EventOptionEntryContract {
    public EventOptionEntryContract() {}

    public static abstract class EventOptionEntry implements BaseColumns {
        public static final String TABLE_NAME = "eventOptions";
        public static final String COLUMN_NAME_EVENT_OPTION_ID = "eventOptionId";
        public static final String COLUMN_NAME_EVENT_OPTION = "eventOptionName";
        public static final String COLUMN_NAME_EVENT_OPTION_WEIGHT = "eventOptionWeight";
        public static final String COLUMN_NAME_EVENT = "event";
    }
}
