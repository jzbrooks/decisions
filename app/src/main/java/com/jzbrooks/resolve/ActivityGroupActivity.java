package com.jzbrooks.resolve;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.text.InputType;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.jzbrooks.resolve.data.DataModel;
import com.jzbrooks.resolve.services.DeletableListAdapter;

import java.util.ArrayList;
import java.util.List;

public class ActivityGroupActivity extends AppCompatActivity {

    private DeletableListAdapter listAdapter;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_group);
        Toolbar toolbar = (Toolbar) findViewById(R.id.mainToolbar);
        setSupportActionBar(toolbar);

        SharedPreferences sharedPreferences = getPreferences(Context.MODE_PRIVATE);
        boolean tutorialDismissedForever = sharedPreferences.getBoolean(getString(R.string.saved_activity_tutorial_state), false);

        if (!tutorialDismissedForever) {
            final Dialog dialog = new Dialog(this);
            dialog.setTitle(R.string.what_now);
            dialog.setContentView(R.layout.tutorial_dialog_layout);

            TextView textView = (TextView) dialog.findViewById(R.id.tutorial_text);
            Button dismissBtn = (Button) dialog.findViewById(R.id.dismiss_btn);
            Button dismissForeverBtn = (Button) dialog.findViewById(R.id.dismiss_forever_btn);

            textView.setText(R.string.activity_group_tutorial);

            dismissBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            dismissForeverBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SharedPreferences sharedPref = getPreferences(Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPref.edit();
                    editor.putBoolean(getString(R.string.saved_activity_tutorial_state), true);
                    editor.apply();

                    dialog.dismiss();
                }
            });

            dialog.show();
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if (DataModel.eventList == null) {
            DataModel.initialize(this);
        }

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(ActivityGroupActivity.this);

                TextView title = new TextView(ActivityGroupActivity.this);
                title.setText(R.string.new_activity_dialog_title);
                title.setPadding(15, 15, 15, 15);
                title.setGravity(Gravity.CENTER_HORIZONTAL);
                title.setTextColor(Color.DKGRAY);
                title.setTextSize(20);
                builder.setCustomTitle(title);

                final EditText input = new EditText(ActivityGroupActivity.this);
                input.setGravity(Gravity.CENTER_HORIZONTAL);
                input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_WORDS);
                builder.setView(input);

                builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        final String message = input.getText().toString();
                        if (!message.isEmpty()) {
                            DataModel.AddActivityGroup(message);
                            listAdapter.notifyDataSetChanged();
                            Snackbar.make(view, String.format(getString(R.string.added_item), message), Snackbar.LENGTH_LONG)
                                    .setAction(R.string.undo, new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            DataModel.DeleteActivityGroup(message);
                                            listAdapter.notifyDataSetChanged();
                                        }
                                    }).show();
                        }
                        else {
                            Snackbar.make(view, R.string.activity_name_cannot_be_empty, Snackbar.LENGTH_LONG).show();
                        }
                    }
                });
                builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

                builder.show().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
            }
        });

        ActionBar bar = getSupportActionBar();
        if (bar != null) {
            bar.setDisplayHomeAsUpEnabled(true);
        }

        RecyclerView mRecyclerView = (RecyclerView) findViewById(R.id.list_view);

        LinearLayoutManager llm = new LinearLayoutManager(this);
        llm.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(llm);

        listAdapter = new DeletableListAdapter(DataModel.eventList);
        listAdapter.setSelectListener(itemSelectedListener);
        mRecyclerView.setAdapter(listAdapter);

        ItemTouchHelper touchHelper = new ItemTouchHelper(itemTouchCallback);
        touchHelper.attachToRecyclerView(mRecyclerView);
    }

    private View.OnClickListener itemSelectedListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            TextView textView = (TextView) v.findViewById(R.id.list_item_text);
            String item = textView.getText().toString();

            Intent intent = new Intent(getApplicationContext(), ActivityOptionActivity.class);
            intent.putExtra("EventGroup", item);
            startActivity(intent);
        }
    };

    ItemTouchHelper.SimpleCallback itemTouchCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.RIGHT | ItemTouchHelper.LEFT) {

        Drawable background;
        Drawable deleteIcon;
        int deleteIconMargin;
        boolean initialized;

        @Override
        public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
            return false;
        }

        @Override
        public void onSwiped(RecyclerView.ViewHolder viewHolder, int swipeDir) {
            TextView textView = ((DeletableListAdapter.DeletableViewHolder)viewHolder).mNameView;

            final String event = textView.getText().toString();
            final List<String> eventOptions = DataModel.DbGetActivityOptions(event);
            final List<String> eventWeights = new ArrayList<String>();
            final int removedPosition = viewHolder.getAdapterPosition();

            for (String eventOption : eventOptions) {
                eventWeights.add(DataModel.DbGetActivityOptionWeight(event, eventOption));
                DataModel.DeleteActivityOption(event, eventOption);
            }

            DataModel.DeleteActivityGroup(event);

            listAdapter.notifyItemRemoved(removedPosition);
            listAdapter.notifyItemRangeChanged(removedPosition, DataModel.eventList.size());

            Snackbar.make(viewHolder.itemView.getRootView().findViewById(R.id.list_view), String.format(getString(R.string.deleted_item), event), Snackbar.LENGTH_LONG)
                    .setAction(R.string.undo, new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            DataModel.AddActivityGroup(event, removedPosition);

                            int count = 0;
                            for (String eventOption : eventOptions) {
                                DataModel.AddActivityOption(event, eventOption, eventWeights.get(count));
                                ++count;
                            }

                            listAdapter.notifyItemInserted(removedPosition);
                            listAdapter.notifyItemRangeChanged(removedPosition, DataModel.eventList.size());
                        }
                    }).show();
        }

        @Override
        public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {

            View itemView = viewHolder.itemView;

            if (viewHolder.getAdapterPosition() == -1) {
                return;
            }

            if (!initialized) {
                intialize();
            }

            int itemHeight = itemView.getBottom() - itemView.getTop();
            int intrinsicWidth = deleteIcon.getIntrinsicWidth();
            int intrinsicHeight = deleteIcon.getIntrinsicWidth();

            int deleteIconTop = itemView.getTop() + (itemHeight - intrinsicHeight)/2;
            int deleteIconBottom = deleteIconTop + intrinsicHeight;

            if (itemView.getLeft() + (int) dX < 0) {
                background.setBounds(itemView.getRight() + (int) dX, itemView.getTop(), itemView.getRight(), itemView.getBottom());
                background.draw(c);

                int deleteIconLeft = itemView.getRight() - deleteIconMargin - intrinsicWidth;
                int deleteIconRight = itemView.getRight() - deleteIconMargin;

                Path clipBounds = new Path();
                clipBounds.addRect((float) itemView.getRight() + dX,
                        (float) deleteIconTop,
                        (float) deleteIconRight,
                        (float) deleteIconBottom,
                        Path.Direction.CCW);

                deleteIcon.setBounds(deleteIconLeft, deleteIconTop, deleteIconRight, deleteIconBottom);
                c.clipPath(clipBounds);
                deleteIcon.draw(c);
            }
            else {
                background.setBounds(itemView.getLeft(), itemView.getTop(), itemView.getLeft() + (int) dX, itemView.getBottom());
                background.draw(c);

                int deleteIconLeft = itemView.getLeft() + deleteIconMargin;
                int deleteIconRight = itemView.getLeft() + deleteIconMargin + intrinsicWidth;

                Path clipBounds = new Path();
                clipBounds.addRect((float) deleteIconLeft,
                        (float) deleteIconTop,
                        (float) itemView.getLeft() + dX,
                        (float) deleteIconBottom,
                        Path.Direction.CCW);

                deleteIcon.setBounds(deleteIconLeft, deleteIconTop, deleteIconRight, deleteIconBottom);
                c.clipPath(clipBounds);
                deleteIcon.draw(c);
            }

            super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
        }

        private void intialize() {
            background = new ColorDrawable(ActivityGroupActivity.this.getResources().
                    getColor(R.color.colorAccent));
            deleteIcon = ContextCompat.getDrawable(ActivityGroupActivity.this, R.drawable.ic_delete_material);
            deleteIcon.setColorFilter(Color.GRAY, PorterDuff.Mode.SRC_ATOP);
            deleteIconMargin = (int) ActivityGroupActivity.this.getResources().getDimension(R.dimen.delete_icon_padding);
            initialized = true;
        }
    };
}
