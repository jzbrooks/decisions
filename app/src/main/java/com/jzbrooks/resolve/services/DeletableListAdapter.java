package com.jzbrooks.resolve.services;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

import com.jzbrooks.resolve.R;

/**
 * Recycler View for Deletable List Items in Resolve
 */
public class DeletableListAdapter extends RecyclerView.Adapter<DeletableListAdapter.DeletableViewHolder> {
    private View.OnClickListener selectListener;
    private View.OnLongClickListener longClickListener;

    public DeletableViewHolder viewHolder;

    List<String> items;

    public DeletableListAdapter(List<String> list)
    {
        items = list;
    }

    @Override
    public DeletableViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).
                inflate(R.layout.deletable_list_item, parent, false);

        if (selectListener != null) view.setOnClickListener(selectListener);
        if (longClickListener != null) view.setOnLongClickListener(longClickListener);

        viewHolder = new DeletableViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(DeletableViewHolder holder, int position) {
        String itemLabel = items.get(position);
        holder.mNameView.setText(itemLabel);
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class DeletableViewHolder extends RecyclerView.ViewHolder {

        public TextView mNameView;

        public DeletableViewHolder(View itemView) {
            super(itemView);

            mNameView = (TextView) itemView.findViewById(R.id.list_item_text);

            itemView.setTag(this);
        }

        public int getListPosition() {
            return getAdapterPosition();
        }
    }

    public void setSelectListener(View.OnClickListener listener) {
        selectListener = listener;
    }
    public void setLongClickListener(View.OnLongClickListener listener) { longClickListener = listener; }
}