package com.jzbrooks.resolve.services;

/**
 * Event option frequency
 */
public class Frequency {
    public static final int LessFrequent = 1;
    public static final int LessFrequentDb = 1;
    public static final int Frequent = 2;
    public static final int FrequentDb = 4;
    public static final int MoreFrequent = 3;
    public static final int MoreFrequentDb = 9;

    public static int convertProgressToDbValue(int progressValue) {
        int dbValue;

        switch (progressValue)
        {
            case Frequency.LessFrequent:
                dbValue = Frequency.LessFrequentDb;
                break;
            case Frequency.MoreFrequent:
                dbValue = Frequency.MoreFrequentDb;
                break;
            case Frequency.Frequent:
            default:
                dbValue = Frequency.FrequentDb;
        }

        return dbValue;
    }

    public static int convertDbValueToProgress(int dbValue) {
        int progressValue;

        switch (dbValue)
        {
            case Frequency.LessFrequentDb:
                progressValue = Frequency.LessFrequent;
                break;
            case Frequency.MoreFrequentDb:
                progressValue = Frequency.MoreFrequent;
                break;
            case Frequency.FrequentDb:
            default:
                progressValue = Frequency.Frequent;
        }

        return progressValue;
    }
}
