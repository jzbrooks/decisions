package com.jzbrooks.resolve;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;

import com.github.paolorotolo.appintro.AppIntro;
import com.github.paolorotolo.appintro.AppIntroFragment;

public class AppIntroActivity extends AppIntro {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setDepthAnimation();

        addSlide(AppIntroFragment.newInstance(getString(R.string.appintro_setup_title), getString(R.string.appintro_setup_message),
                R.drawable.appintro_add, getResources().getColor(R.color.colorPrimaryDark)));
        addSlide(AppIntroFragment.newInstance(getString(R.string.appintro_roll_title), getString(R.string.appintro_roll_message),
                R.drawable.appintro_roll, getResources().getColor(R.color.colorAccentDark)));
        addSlide(AppIntroFragment.newInstance(getString(R.string.appintro_share_title), getString(R.string.appintro_share_message),
                R.drawable.appintro_share, getResources().getColor(R.color.colorPrimaryAlternate)));

        showStatusBar(false);
    }

    @Override
    public void onSkipPressed(Fragment currentFragment) {
        super.onSkipPressed(currentFragment);

        Intent intent = new Intent(AppIntroActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onDonePressed(Fragment currentFragment) {
        super.onDonePressed(currentFragment);

        Intent intent = new Intent(AppIntroActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }
}
