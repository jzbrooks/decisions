package com.jzbrooks.resolve;

import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.app.AlertDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.view.ViewCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.OvershootInterpolator;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import com.jzbrooks.resolve.data.DataModel;

import java.text.DateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.ThreadLocalRandom;

@SuppressWarnings("ConstantConditions")
public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, View.OnClickListener {

    static Context _context;
    private String mMessage;
    private float mFabRotation = 90f;

    private String LOG_TAG = this.getClass().getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //  Declare a new thread to do a preference check
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                //  Initialize SharedPreferences
                SharedPreferences getPrefs = PreferenceManager
                        .getDefaultSharedPreferences(getBaseContext());

                //  Create a new boolean and preference and set it to true
                boolean isFirstStart = getPrefs.getBoolean("firstStart", true);

                //  If the activity has never started before...
                if (isFirstStart) {

                    //  Launch app intro
                    Intent i = new Intent(MainActivity.this, AppIntroActivity.class);
                    startActivity(i);
                    finish();

                    //  Make a new preferences editor
                    SharedPreferences.Editor e = getPrefs.edit();

                    //  Edit preference to make it false because we don't want this to run again
                    e.putBoolean("firstStart", false);

                    //  Apply changes
                    e.apply();
                }
            }
        });
        t.run();

        setContentView(R.layout.activity_main);
        
        Toolbar toolbar = (Toolbar) findViewById(R.id.mainToolbar);
        assert toolbar != null;
        setSupportActionBar(toolbar);

        _context = getApplicationContext();

        Window window = this.getWindow();
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.setStatusBarColor(ContextCompat.getColor(this, R.color.colorPrimaryDark));

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        assert drawer != null;
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        // setup click listeners
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        assert fab != null;
        fab.setOnClickListener(this);

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        assert navigationView != null;
        navigationView.setNavigationItemSelectedListener(this);

        TextView eventView = (TextView) findViewById(R.id.main_event_view);
        assert eventView != null;
        eventView.setOnClickListener(this);

        TextView timeView = (TextView) findViewById(R.id.main_time_view);
        assert timeView != null;
        timeView.setOnClickListener(this);
    }

    @Override
    public void onResume()
    {
        super.onResume();

        DataModel.initialize(getAppContext());

        TextView timeView = (TextView) findViewById(R.id.main_time_view);
        assert timeView != null;
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.MINUTE, 30);
        String currentTime = DateFormat.getTimeInstance(DateFormat.SHORT).format(calendar.getTime());
        timeView.setText(currentTime);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        assert drawer != null;
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_reset) {
            new AlertDialog.Builder(this)
                    .setTitle(R.string.reset_lists)
                    .setMessage(R.string.reset_list_warning)
                    .setPositiveButton(R.string.yes, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            DataModel.ResetDatastores();
                            TextView eventView = (TextView) findViewById(R.id.main_event_view);
                            TextView resultView = (TextView) findViewById(R.id.main_result_view);
                            eventView.setText(getResources().getString(R.string.activity_prompt));
                            resultView.setText(R.string.result_placeholder);

                            LinearLayout shareButton = (LinearLayout) findViewById(R.id.main_share_button);
                            if (shareButton.getVisibility() == View.VISIBLE) shareButton.setVisibility(View.INVISIBLE);
                        }
                    })
                    .setNegativeButton(R.string.no, null)
                    .show();
        }
        else if (id == R.id.action_show_tutorials) {
            //  Declare a new thread to do a preference check
            Thread t = new Thread(new Runnable() {
                @Override
                public void run() {
                    Intent intent = new Intent(MainActivity.this, AppIntroActivity.class);
                    startActivity(intent);
                    finish();
                }
            });
            t.run();
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_events) {
            Intent intent = new Intent(this, ActivityGroupActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_groups) {
            if (DataModel.eventList.isEmpty()) {
                Snackbar.make(findViewById(R.id.main_parent_view), R.string.add_activities, Snackbar.LENGTH_LONG)
                        .setAction(getString(R.string.take_me), new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent intent = new Intent(_context, ActivityGroupActivity.class);
                                startActivity(intent);
                            }
                        }).show();
            } else {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                final CharSequence[] listContent = DataModel.eventList.toArray(new CharSequence[DataModel.eventList.size()]);
                builder.setTitle(R.string.select_activity)
                        .setItems(listContent, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                Intent intent = new Intent(_context, ActivityOptionActivity.class);
                                intent.putExtra("EventGroup", listContent[which]);
                                startActivity(intent);
                            }
                        });
                builder.show();
            }
        } else if (id == R.id.nav_about) {
            Intent intent = new Intent(this, AboutActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_support) {
            Uri uri = Uri.parse(getString(R.string.support_url));
            Intent intent = new Intent(Intent.ACTION_VIEW, uri);
            startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        assert drawer != null;
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onClick(View v) {
        int id = v.getId();
        final View _VIEW = v;

        switch (id) {
            case R.id.main_time_view:
                ShowUpdateTimeDialog();
                break;
            case R.id.main_event_view:
                if (DataModel.eventList.isEmpty()) {
                    Snackbar.make(_VIEW, getString(R.string.add_activities), Snackbar.LENGTH_LONG)
                            .setAction(getString(R.string.take_me), new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent intent = new Intent(_context, ActivityGroupActivity.class);
                                    startActivity(intent);
                                }
                            }).show();
                }
                else {
                    AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    CharSequence[] listContent = DataModel.eventList.toArray(new CharSequence[DataModel.eventList.size()]);
                    builder.setTitle(getString(R.string.select_activity))
                            .setItems(listContent, new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    TextView textView = (TextView) _VIEW;
                                    textView.setText(DataModel.eventList.get(which));
                                }
                            });
                    builder.show();
                }
                break;
            case R.id.main_share_button:
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, mMessage);
                sendIntent.setType("text/plain");
                startActivity(sendIntent);
                break;
            case R.id.fab:
                FloatingActionButton floatingActionButton = (FloatingActionButton) v.findViewById(R.id.fab);
                setupFab(v, floatingActionButton);
                break;
            default:
        }
    }

    private void ShowUpdateTimeDialog() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);
        TimePickerDialog timePicker = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                Calendar calendar = Calendar.getInstance();
                calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
                calendar.set(Calendar.MINUTE, minute);
                calendar.clear(Calendar.SECOND);

                String date = DateFormat.getTimeInstance(DateFormat.SHORT).format(calendar.getTime());

                TextView timeView = (TextView) findViewById(R.id.main_time_view);
                assert timeView != null;
                timeView.setText(date);
            }
        }, hour, minute, false);

        timePicker.show();
    }

    private void setupFab(View view, FloatingActionButton fab) {
        TextView eventView = (TextView) findViewById(R.id.main_event_view);
        TextView timeView = (TextView) findViewById(R.id.main_time_view);
        TextView resultView = (TextView) findViewById(R.id.main_result_view);
        ArrayList<String> optionsList = new ArrayList<String>();
        assert eventView != null;
        assert timeView != null;

        final String eventGroup = eventView.getText().toString();

        String time = timeView.getText().toString();
        if (eventGroup.equals(getResources().getString(R.string.activity_prompt))) {
            Snackbar.make(view, R.string.yo_select_an_activity, Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
            return;
        }

        //build weighted list
        int uniqueOptionCount = DataModel.eventOptionList.get(eventGroup).size();
        for (int i=0; i<uniqueOptionCount; ++i) {
            String currentOption = DataModel.eventOptionList.get(eventGroup).get(i);
            int itemWeight = Integer.parseInt(DataModel.DbGetActivityOptionWeight(eventGroup, currentOption));
            for (int j=0; j<itemWeight; ++j) {
                optionsList.add(currentOption);
            }
        }

        if (!optionsList.isEmpty()) {
            int rand = ThreadLocalRandom.current().nextInt(0, optionsList.size());
            String choice = optionsList.get(rand);

            assert resultView != null;
            resultView.setText(choice);
            animateTextViewTextColor(resultView);

            animateFab(fab);

            LinearLayout shareButton = (LinearLayout) findViewById(R.id.main_share_button);
            assert shareButton != null;
            if (shareButton.getVisibility() == View.GONE) {
                shareButton.setVisibility(View.VISIBLE);
                shareButton.setOnClickListener(MainActivity.this);
            }

            mMessage = String.format(getString(R.string.share_message_content), choice, eventGroup, time);
        } else {
            Snackbar.make(view, String.format(getString(R.string.add_options), eventGroup), Snackbar.LENGTH_LONG)
                    .setAction(R.string.take_me, new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(getApplicationContext(), ActivityOptionActivity.class);
                            intent.putExtra("EventGroup", eventGroup);
                            startActivity(intent);
                        }
                    }).show();
        }
    }

    private void animateFab(FloatingActionButton fab) {
        final OvershootInterpolator interpolator = new OvershootInterpolator();
        ViewCompat.animate(fab).rotation(mFabRotation).withLayer().setDuration(300).setInterpolator(interpolator).start();

        mFabRotation += 90f;
    }

    private void animateTextViewTextColor(TextView textView) {
        ObjectAnimator animator = ObjectAnimator.ofInt(textView, "textColor",
                ContextCompat.getColor(_context, R.color.colorAccent),
                ContextCompat.getColor(_context, android.R.color.tertiary_text_light));
        animator.setDuration(750);
        animator.setEvaluator(new ArgbEvaluator());
        animator.start();
    }

    public static Context getAppContext() {
        return _context;
    }
}
