package com.jzbrooks.decisions;

import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.jzbrooks.resolve.MainActivity;
import com.jzbrooks.resolve.R;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.Espresso.pressBack;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.replaceText;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.doesNotExist;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.contrib.DrawerActions.open;
import static android.support.test.espresso.contrib.DrawerMatchers.isOpen;
import static android.support.test.espresso.matcher.ViewMatchers.withClassName;
import static android.support.test.espresso.matcher.ViewMatchers.withContentDescription;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static com.jzbrooks.decisions.TestOperations.invalidateView;
import static org.hamcrest.Matchers.endsWith;
import static org.hamcrest.Matchers.hasToString;
import static org.hamcrest.Matchers.startsWith;

@RunWith(AndroidJUnit4.class)
public class SnackbarUndoTest {

    private static final String testEventName = "Game Night";
    private static final String[] testEventOptions = {"Catan", "Exploding Kittens", "Munchkin"};
    private static final Integer[] testEventWeights = {2, 2, 3};

    @Rule
    public ActivityTestRule mActivityRule = new ActivityTestRule(MainActivity.class);

    @Test
    public void undoAddEvent() {
        TestOperations.resetLists();

        onView(withId(R.id.drawer_layout)).perform(open());
        onView(withId(R.id.drawer_layout)).check(matches(isOpen()));

        onView(withText(R.string.actionbar_title_activity_group)).perform(click());

        onView(withId(R.id.fab))
                .perform(click());

        onView(withClassName(endsWith("EditText"))).perform(typeText(testEventName));
        onView(withText(R.string.ok)).perform(click());

        onView(withText(R.string.undo)).perform(click());

        onView(withText(testEventName))
                .check(doesNotExist());

        pressBack();

        TestOperations.resetLists();
    }

    @Test
    public void undoAddEventOption() {
        TestOperations.resetLists();

        onView(withId(R.id.drawer_layout)).perform(open());
        onView(withId(R.id.drawer_layout)).check(matches(isOpen()));

        onView(withText(R.string.actionbar_title_activity_group)).perform(click());

        onView(withId(R.id.fab))
                .perform(click());

        onView(withClassName(endsWith("EditText"))).perform(typeText(testEventName));
        onView(withText(R.string.ok)).perform(click());

        onView(withId(R.id.list_view)).perform(
                RecyclerViewActions.actionOnItemAtPosition(0, click()));

        for (int i = 0; i < testEventOptions.length; ++i) {
            onView(withId(R.id.fab))
                    .perform(click());

            onView(withId(R.id.optionName)).perform(click(), typeText(testEventOptions[i]));
            onView(withId(R.id.optionFreq)).perform(TestOperations.setProgress(testEventWeights[i]));
            onView(withText(R.string.save)).perform(click());
        }

        onView(withText(R.string.undo)).perform(click());

        onView(withText(testEventOptions[2]))
                .check(doesNotExist());

        onView(withContentDescription(R.string.abc_action_bar_up_description)).perform(click());
        onView(withContentDescription(R.string.abc_action_bar_up_description)).perform(click());
        TestOperations.resetLists();
    }

    @Test
    public void undoModifyEventOption() {

    }
}
