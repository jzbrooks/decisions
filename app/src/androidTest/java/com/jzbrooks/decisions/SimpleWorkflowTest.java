package com.jzbrooks.decisions;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.jzbrooks.resolve.MainActivity;
import com.jzbrooks.resolve.R;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withContentDescription;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.not;


@RunWith(AndroidJUnit4.class)
public class SimpleWorkflowTest {

    @Rule
    public ActivityTestRule mActivityRule = new ActivityTestRule(MainActivity.class);

    @Test
    public void simpleWorkflowTest() {
        TestOperations.resetLists();

        tryAddEvent();
        tryRollWithoutSelectedEvent();
        tryRollWithSelection();

        TestOperations.resetLists();
    }

    public void tryAddEvent() {
        TestOperations.seedData();

        //pressBack();
        //pressBack();
        onView(withContentDescription(R.string.abc_action_bar_up_description)).perform(click());
        onView(withContentDescription(R.string.abc_action_bar_up_description)).perform(click());
    }

    public void tryRollWithoutSelectedEvent() {
        onView(withId(R.id.fab))
                .perform(click());

        onView(allOf(withId(android.support.design.R.id.snackbar_text), withText(R.string.yo_select_an_activity)))
                .check(matches(isDisplayed()));
    }

    public void tryRollWithSelection() {
        onView(withId(R.id.main_event_view))
                .perform(click());

        onView(withText(TestOperations.testEventName))
                .perform(click());

        onView(withId(R.id.fab))
                .perform(click());

        onView(withId(R.id.main_result_view))
                .check(matches(not(withText(R.string.result_placeholder))));

        onView(withId(R.id.main_share_button))
                .check(matches(isDisplayed()));
    }
}