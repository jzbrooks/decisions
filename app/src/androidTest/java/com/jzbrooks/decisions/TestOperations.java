package com.jzbrooks.decisions;

import android.support.test.espresso.UiController;
import android.support.test.espresso.ViewAction;
import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.espresso.matcher.ViewMatchers;
import android.view.View;

import com.jzbrooks.resolve.R;

import org.adw.library.widgets.discreteseekbar.DiscreteSeekBar;
import org.hamcrest.Matcher;

import static android.support.test.InstrumentationRegistry.getInstrumentation;
import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.Espresso.openActionBarOverflowOrOptionsMenu;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.replaceText;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.contrib.DrawerActions.open;
import static android.support.test.espresso.contrib.DrawerMatchers.isOpen;
import static android.support.test.espresso.matcher.ViewMatchers.withClassName;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.endsWith;
import static org.hamcrest.Matchers.hasToString;
import static org.hamcrest.Matchers.startsWith;

/**
 * Espresso Test Common Utility Methods
 */
public class TestOperations {

    public static final String testEventName = "Lunch";
    public static final String[] testEventOptions = {"Taco Bell", "Panera", "ChicFilA"};
    public static final Integer[] testEventWeights = {1, 2, 3};

    public static void seedData() {
        onView(withId(R.id.drawer_layout)).perform(open());
        onView(withId(R.id.drawer_layout)).check(matches(isOpen()));

        onView(withText(R.string.actionbar_title_activity_group)).perform(click());

        onView(withId(R.id.fab))
                .perform(click());

        onView(withClassName(endsWith("EditText"))).perform(typeText(testEventName));
        onView(withText(R.string.ok)).perform(click());

        onView(withId(R.id.list_view)).perform(
                RecyclerViewActions.actionOnItemAtPosition(0, click()));

        for (int i = 0; i < testEventOptions.length; ++i) {
            onView(withId(R.id.fab))
                    .perform(click());

            onView(withId(R.id.optionName)).perform(click(), typeText(testEventOptions[i]));
            onView(withId(R.id.optionFreq)).perform(setProgress(testEventWeights[i]));
            onView(withText(R.string.save)).perform(click());
            onView(withId(R.id.list_view)).perform(invalidateView());
        }
    }


    public static ViewAction invalidateView() {
        return new ViewAction() {
            @Override
            public void perform(UiController uiController, View view) {
                view.invalidate();
            }

            @Override
            public String getDescription() {
                return "cause view to redraw";
            }

            @Override
            public Matcher<View> getConstraints() {
                return ViewMatchers.isAssignableFrom(View.class);
            }
        };
    }


    public static ViewAction setProgress(final int progress) {
        return new ViewAction() {
            @Override
            public void perform(UiController uiController, View view) {
                ((DiscreteSeekBar) view).setProgress(progress);
            }

            @Override
            public String getDescription() {
                return "Set a progress";
            }

            @Override
            public Matcher<View> getConstraints() {
                return ViewMatchers.isAssignableFrom(DiscreteSeekBar.class);
            }
        };
    }

    public static void resetLists() {
        openActionBarOverflowOrOptionsMenu(getInstrumentation().getTargetContext());
        onView(withText(R.string.reset)).perform(click());

        try {
            Thread.sleep(400);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        onView(withText(R.string.yes)).perform(click());

        try {
            Thread.sleep(400);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
