# README #

## What is this repository for? ##

### Resolve ###
The group decision maker app.

Indecision is no more.
Resolve is a simple way to make a random decision and communicate it to your friends. 
Add events and options and let Resolve decide for you, then share with any messaging app!
Add some events to your event list, add some options for the events, and then from the main screen select the event you can't seem to agree upon and tap the unbiased roll button. Your decision has been made.

## How do I get set up? ##

* Android Studio 2.1
* Android N SDK
* Java 8

## Contribution guidelines ##

* Create and/or assign an issue to yourself
* Unit tests
* Espresso tests
* Code review

## Who do I talk to? ##

* [ Justin Brooks ](mailto:jstnz72@gmail.com)

[ ![google-play-badge.png](https://bitbucket.org/repo/945edz/images/2361517579-google-play-badge.png) ](https://play.google.com/store/apps/details?id=com.jzbrooks.decisions)